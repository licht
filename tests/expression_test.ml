(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2
module T = Tools

let u = UTF8.from_utf8string

let _msg ~expected ~result =

    Printf.sprintf "Expected %s but got %s"
    (UTF8.raw_encode @@ Expression.show expected)
    (UTF8.raw_encode @@ Expression.show result)

let assert_equal expected result =
  OUnit2.assert_equal
    ~cmp:(Expression.(=))
    ~msg:(_msg ~expected ~result)
    expected result


let test_str ctx = begin
  let result = Expression.load @@ u"cafe" in
  let expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.string (u"cafe")) in
  assert_equal expected result
end

(** If the string start with space, it should be interpreted as a litteral *)
let test_str_space ctx = begin
  let result = Expression.load @@ u" =cafe" in
  let expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.string (u" =cafe")) in
  assert_equal expected result
end

let test_formula_str ctx = begin
  let result = Expression.load @@ u"=\"cafe\"" in
  let expected = Expression.load_expr @@ Expression.Formula (
    Expression.Expression (
      ScTypes.Expr.value (
        ScTypes.Type.string (u"cafe")))) in
  assert_equal expected result
end

let test_num ctx = begin
  let result = Expression.load @@ u"123" in
  let expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.number (
      DataType.Num.of_int 123
    )) in
  assert_equal expected result
end

let test_float ctx = begin
  let result = Expression.load @@ u"12.45" in
  let expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.number (
      DataType.Num.of_float @@ float_of_string "12.45"
    )) in
  assert_equal expected result
end

let test_relative ctx = begin
  let result = Expression.load @@ u"-123" in
  let expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.number (
      DataType.Num.of_int (-123)
    )) in
  assert_equal expected result
end

let test_date ctx = begin
  let result = Expression.load @@ u"1900/01/01"
  and expected = Expression.load_expr @@ Expression.Basic (
    ScTypes.Type.date (
      DataType.Date.get_julian_day 1900 01 01
    )) in
  assert_equal expected result
end

let test_sources ctx = begin
  let result = Expression.load @@ u"=A1"
             |> Expression.collect_sources in

  let expected = Cell.Set.singleton (1, 1) in

  let msg = Printf.sprintf "Expected %s but got %s"
    (UTF8.raw_encode @@ Tools.String.print_buffer Cell.Set.printb expected)
    (UTF8.raw_encode @@ Tools.String.print_buffer Cell.Set.printb result) in

  OUnit2.assert_equal ~msg expected result
end

let test_sources2 ctx = begin
  let result = Expression.load @@ u"=if($A$1>0;rand()*10+1;0)"
            |> Expression.collect_sources
  and expected = Cell.Set.singleton (1, 1) in

  let msg_buffer = UTF8.Buffer.create 16 in
  UTF8.Printf.bprintf msg_buffer "Expected %a but got %a"
    Cell.Set.printb expected
    Cell.Set.printb result;

  OUnit2.assert_equal
    ~msg:(UTF8.to_utf8string @@ UTF8.Buffer.contents msg_buffer)
    expected
    result

end

let tests = "expression_test">::: [
  (* Test litteral input *)
  "test_str"            >:: test_str;
  "test_str_space"      >:: test_str_space;

  "test_formula_str"    >:: test_formula_str;

  (* Test numeric input *)
  "test_num"            >:: test_num;
  "test_float"          >:: test_float;
  "test_relative"       >:: test_relative;

  "test_date"           >:: test_date;

  "test_sources"        >:: test_sources;
  "test_sources2"       >:: test_sources2;
]
