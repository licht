(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2

let _msg ~expected ~result =
    Printf.sprintf "Expected %s but got %s"
    (String.escaped expected)
    (String.escaped result)

let test_decode env = begin
  let result = Unicode.decode ~encoding:`ISO_8859_1 "caf\xE9"
            |> Unicode.to_utf8
  and expected = "café" in

  assert_equal
      ~msg:(_msg ~expected ~result)
    expected
    result
end

let test_tolist env = begin


  let result = Unicode.decode ~encoding:`UTF_8 "café"
            |> Unicode.to_list in

  let expected = ['c'; 'a'; 'f'; '\xE9']
              |> List.map Uchar.of_char in

  assert_equal
    expected
    result

end


let tests = "unicode_test">::: [
  "test_decode" >:: test_decode;
  "test_tolist" >:: test_tolist

]
