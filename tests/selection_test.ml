(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2
module S = Selection

(* Test if a single cell is selected with an axe *)
let test_selected_single axe1 axe2 env = begin

  let selection = Selection.create (10, 10) in

  assert_equal
    true
    (Selection.is_selected axe1 selection);

  assert_equal
    false
    (Selection.is_selected axe2 selection);
end

let test_selected_multiple axe1 axe2 env = begin

  let selection = Selection.extends
    (Actions.Left 1)
    (Selection.create (10, 10)) in

  assert_equal
    true
    (Selection.is_selected axe1 selection);

  assert_equal
    false
    (Selection.is_selected axe2 selection);
end

let test_extract_single env = begin

  let selection = Selection.create (10, 10) in

  assert_equal
    (10, 10)
    (Selection.extract selection);
end
let test_extract_multiple env = begin

  let selection = Selection.extends
    (Actions.Left 1)
    (Selection.create (10, 10)) in

  assert_equal
    (9, 10)
    (Selection.extract selection);
end

let tests = "selection_test">::: [
  "test_selected1" >:: test_selected_single (S.Horizontal 10) (S.Horizontal 11);
  "test_selected2" >:: test_selected_single (S.Vertical 10)   (S.Vertical 11);
  "test_selected3" >:: test_selected_single (S.Cell (10, 10)) (S.Cell (11, 11));
  "test_selected4" >:: test_selected_multiple (S.Horizontal 10) (S.Horizontal 11);
  "test_selected5" >:: test_selected_multiple (S.Vertical 10)   (S.Vertical 11);
  "test_selected6" >:: test_selected_multiple (S.Cell (10, 10)) (S.Cell (11, 11));
  "test_extract1 " >:: test_extract_single;
  "test_extract2 " >:: test_extract_multiple;
]
