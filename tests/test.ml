(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

let () =

  (*Evaluator.set_catalog (Functions.C.compile @@ Functions.built_in @@ Functions.C.empty);*)

  let tests = OUnit2.test_list [
    Tools_test.tests;
    DataType_test.num_tests;
    ExpressionParser_test.tests;
    Expression_test.tests;
    Sheet_test.tests;
    Odf_ExpressionParser_test.tests;
    Selection_test.tests;
    Splay_test.tests;
    Evaluate_test.tests;
    Show_expr_test.tests;
  ]
  in OUnit2.run_test_tt_main tests


