let u = UTF8.from_utf8string

module M(E:Sym_expr.SYM_EXPR) = struct

  let string =
    let v = E.value (E.T.str @@ u "This is a test string") in
    E.observe v

  let date0 =
    let zero = E.value (E.T.date @@ DataType.Num.zero) in
    E.observe zero

  let true0 =
    let v = E.call0 (u "true") in
    E.observe v

  let f3 =
    let zero = E.value (E.T.num @@ DataType.Num.zero) in
    let v = E.call3 (u "register3") zero zero zero in
    E.observe v

  let calln =
    let zero = E.value (E.T.num @@ DataType.Num.zero) in
    let one = E.value (E.T.num @@ DataType.Num.one) in
    E.observe (E.callN (u "calln") [zero; one])

end
