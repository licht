open OUnit2

open Functions
let f_string = ScTypes.ReturnType.f_string

let u = UTF8.from_utf8string

let _msg ~expected ~result =
    Printf.sprintf "Expected %s but got %s"
    (UTF8.raw_encode expected)
    (UTF8.raw_encode result)

module M = BuildExpression.M(Show_expr.Show_Expr(Show_ref)(Show_type))

let test_value_string e = begin
  let buffer = UTF8.Buffer.create 16 in

  let expected = u "This is a test string"
  and result = M.string buffer; UTF8.Buffer.contents buffer in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected result

end

let test_value_date e = begin

  let buffer = UTF8.Buffer.create 16 in
  let expected = u "1899/12/30"
  and result = M.date0 buffer; UTF8.Buffer.contents buffer in

    assert_equal
    ~msg:(_msg ~expected ~result)
    expected result
end

let test_evaluate0 e = begin
  let buffer = UTF8.Buffer.create 16 in
  let expected = u "true()"
  and result = M.true0 buffer; UTF8.Buffer.contents buffer in
  assert_equal
    ~msg:(_msg ~expected ~result)
    expected result
end

let test_evaluate3 e = begin
  let buffer = UTF8.Buffer.create 16 in
  let expected = u "register3(0;0;0)"
  and result = M.f3 buffer; UTF8.Buffer.contents buffer in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected result
end

let test_calln e = begin
  let buffer = UTF8.Buffer.create 16 in
  let expected = u "calln(0;1)"
  and result = M.calln buffer; UTF8.Buffer.contents buffer in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected result
end

let tests = "show_expr_test">::: [
  "test_value_string"       >:: test_value_string;
  "test_value_date"         >:: test_value_date;
  "test_evaluate0"          >:: test_evaluate0;
  "test_evaluate3"          >:: test_evaluate3;
  "test_calln"              >:: test_calln;
]


