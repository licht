open OUnit2

open Functions
let f_string = ScTypes.ReturnType.f_string

let u = UTF8.from_utf8string

let catalog = Functions.C.compile (
    Functions.built_in Functions.C.empty
 |> Functions.C.register3 "register3"  (t_int, t_int, t_int) f_string
        (fun a b c -> u "register3")
)

let _msg ~(expected:ScTypes.Result.t) ~(result:ScTypes.Result.t) =
    Printf.sprintf "Expected %s but got %s"
    (UTF8.raw_encode @@ ScTypes.Result.show expected)
    (UTF8.raw_encode @@ ScTypes.Result.show result)

module M = BuildExpression.M(Evaluate)

let test_value_string e = begin
  assert_equal
    (ScTypes.Result.Ok (ScTypes.Type.string @@ u "This is a test string"))
    (M.string (catalog, fun _ -> None))
end

let test_value_date e = begin

  let expected = ScTypes.Result.Ok (ScTypes.Type.date DataType.Num.zero)
  and result = M.date0 (catalog, fun _ -> None) in

    assert_equal
    ~msg:(_msg ~expected ~result)
    expected result
end

let test_evaluate0 e = begin
  assert_equal
    (ScTypes.Result.Ok (ScTypes.Type.boolean true))
    (M.true0 (catalog, fun _ -> None))
end

let test_evaluate3 e = begin
  assert_equal
    (ScTypes.Result.Ok (ScTypes.Type.string @@ u "register3"))
    (M.f3 (catalog, fun _ -> None))
end

let tests = "evaluate_test">::: [
  "test_value_string"       >:: test_value_string;
  "test_value_date"         >:: test_value_date;
  "test_evaluate0"          >:: test_evaluate0;
  "test_evaluate3"          >:: test_evaluate3;
]

