(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2

let u = UTF8.from_utf8string

module Show = ScTypes.Expr.Eval(Show_expr.Show_Expr(Show_ref)(Show_type))

let _msg ~(expected:ScTypes.Expr.t) ~(result:ScTypes.Expr.t) =
    let b1 = UTF8.Buffer.create 16
    and b2 = UTF8.Buffer.create 16 in
    Show.eval expected b1;
    Show.eval result b2;

    Printf.sprintf "Expected \n\t%s but got \n\t%s"
      (UTF8.raw_encode @@ UTF8.Buffer.contents b1)
      (UTF8.raw_encode @@ UTF8.Buffer.contents b2)



let build_num value = ScTypes.Type.number (
  DataType.Num.of_int value
)


let test_formula ctx = begin

  let test1 = "of:=CONCATENATE(SUM([.F16:.AJ16]);\"/\";8*NETWORKDAYS([.F6]; [.F6]+(ORG.OPENOFFICE.DAYSINMONTH([.F6])-1)))" in

  let line = Lexing.from_string test1 in
  let result = Odf_ExpressionParser.value Odf_ExpressionLexer.read line in

  let expected = ScTypes.(

    Expr.call3 (u"CONCATENATE")
      (Expr.call1 (u"SUM")
        (Expr.ref (Refs.range ((6, 16), (false, false)) (((36, 16), (false, false))))))
      (Expr.value (Type.string (u"/")))
      (Expr.call2 (u"*")
        (Expr.value (build_num 8))
        (Expr.call2 (u"NETWORKDAYS")
          (Expr.ref (Refs.cell ((6, 6), (false, false))))
          (Expr.call2 (u"+")
            (Expr.ref (Refs.cell ((6, 6), (false, false))))
            (Expr.expression
              (Expr.call2  (u"-")
                (Expr.call1 (u"ORG.OPENOFFICE.DAYSINMONTH")
                  (Expr.ref (Refs.cell ((6, 6), (false, false)))))
                (Expr.value (build_num 1));
              )))))) in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result

end

let test_formula2 ctx = begin
  let value = "of:=+[.H51]*[.G52]" in
  let line = Lexing.from_string value in
  let result = Odf_ExpressionParser.value Odf_ExpressionLexer.read line in

  let expected = ScTypes.(

    Expr.call1 (u"+")
      (Expr.call2 (u"*")
        (Expr.ref (Refs.cell ((8, 51), (false, false))))
        (Expr.ref (Refs.cell ((7, 52), (false, false))))
      )) in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result

end

let tests = "odf_ExpressionParser_test" >::: [

  "test_formula"              >:: test_formula;
  "test_formula2"             >:: test_formula2;

 ]
