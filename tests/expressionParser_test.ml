(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2

let u = UTF8.from_utf8string

let _msg ~(expected:Expression.t) ~(result:Expression.t) =
    Printf.sprintf "Expected %s but got %s"
    (UTF8.raw_encode @@ Expression.show @@ Expression.load_expr expected)
    (UTF8.raw_encode @@ Expression.show @@ Expression.load_expr result)

let load_expr str =
  Expression.Formula (
    Expression.Expression(
      ExpressionParser.value ExpressionLexer.read
      @@ Lexing.from_string str )
  )


let test_num ctx = begin

  let expected = Expression.Formula (
    Expression.Expression (
      ScTypes.Expr.value (ScTypes.Type.number (
          DataType.Num.of_int 1
        )
      ))) in
  let result = load_expr "=1" in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result
end

let test_call ctx = begin

  let expected = Expression.Formula (
    Expression.Expression (
      ScTypes.Expr.call0 (u"sum"))) in
  let result = load_expr "=sum()" in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result
end

let test_call2 ctx = begin

  (*  The expression "foo2(" has to be parsed as a function call, and not as a
      reference to cell "FOO2", followed by another expression. *)

  let expected = Expression.Formula (
    Expression.Expression (
      ScTypes.Expr.call1
        (u"foo2")  (ScTypes.Expr.value (ScTypes.Type.number (
          DataType.Num.of_int 4)
        )))) in
  let result = load_expr "=foo2(4)" in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result
end

let test_ref ctx = begin

  let expected = Expression.Formula (
    Expression.Expression (
      ScTypes.Expr.ref(
        ScTypes.Refs.cell ((1, 3), (false, false))))) in
  let result = load_expr "=A3" in

  assert_equal
    ~msg:(_msg ~expected ~result)
    expected
    result
end

let tests = "expression_parser_test">::: [
  "test_num"    >:: test_num;
  "test_call"   >:: test_call;
  "test_call2"  >:: test_call2;
  "test_ref"    >:: test_ref;
]
