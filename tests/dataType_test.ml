(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2
module N = DataType.Num

let test_num_add n1 n2 result ctx = begin
  assert_equal
    ~cmp:(=)
    result
    (N.to_int @@ N.add n1 n2)
end

let test_num_mult n1 n2 result ctx = begin
  assert_equal
    ~cmp:(=)
    result
    (N.to_int @@ N.mult n1 n2)
end

let test_num_sub n1 n2 result ctx = begin
  assert_equal
    ~cmp:(=)
    result
    (N.to_int @@ N.sub n1 n2)
end

let test_round func number expected ctx = begin
  let result = N.to_int @@ func number in

  assert_equal
    ~msg:(Printf.sprintf "Expected %d but got %d" expected result)
    ~cmp:(=)
    expected
    result
end

let n1 = N.of_int 1
let n2 = N.of_int 2

let num_tests = "num_test">::: [

    "test_add"          >:: test_num_add n1     n1    2;
    "test_mult"         >:: test_num_mult n2    n1    2;
    "test_sub"          >:: test_num_sub n1     n1    0;

    "test_floor1"       >:: test_round N.floor      (N.of_float   1.2)    1;
    "test_floor2"       >:: test_round N.floor      (N.of_float (-1.2)) (-2);
    "test_floor3"       >:: test_round N.floor      (N.of_float   1.8)    1;
    "test_floor4"       >:: test_round N.floor      (N.of_float (-1.8)) (-2);

    "test_round_down1"  >:: test_round N.round_down (N.of_float   1.2)    1;
    "test_round_down2"  >:: test_round N.round_down (N.of_float (-1.2)) (-1);
    "test_round_down3"  >:: test_round N.round_down (N.of_float   1.8)    1;
    "test_round_down4"  >:: test_round N.round_down (N.of_float (-1.8)) (-1);

    "test_round"        >:: test_round N.round      (N.of_float   1.2)    1;
    "test_round"        >:: test_round N.round      (N.of_float (-1.2)) (-1);
    "test_round"        >:: test_round N.round      (N.of_float   1.8)    2;
    "test_round"        >:: test_round N.round      (N.of_float (-1.8)) (-2);
 ]
