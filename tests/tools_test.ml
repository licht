(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

open OUnit2

module TestString = struct

  let _msg ~expected ~result =
      Printf.sprintf "Expected %s but got %s"
      expected
      result

  let test_string_of_ints ctx = begin

    let result = Tools.String.string_of_ints 127 in
    let expected = "\127" in

    assert_equal
      ~msg:(_msg ~expected ~result)
      expected
      result
  end

  let test_string_of_ints_512 ctx = begin

    let result = Tools.String.string_of_ints 512 in
    let expected = "\002\000" in

    assert_equal
      ~msg:(_msg ~expected ~result)
      expected
      result
  end

  let test_split ctx = begin
    let result = Tools.String.split ~by:' ' "abc 123 456"
    and expected = ("abc", "123 456") in
    assert_equal expected result
  end

  let test_filter_float src expected ctx = begin

    let result = Tools.String.filter_float src in
    assert_equal
      ~msg:(_msg ~expected ~result)
      expected
      result

  end

  let tests = "string_test">::: [

      "test_string_of_ints"       >:: test_string_of_ints;
      "test_string_of_ints_512"   >:: test_string_of_ints_512;
      "test_string_split"         >:: test_split;

      "filter_float"              >:: test_filter_float "12."  "12";
      "filter_float2"             >:: test_filter_float "12.5" "12.5";
      "filter_float_empty"        >:: test_filter_float "" "";
      "filter_float_no_dot"       >:: test_filter_float "123" "123";
  ]
end

module TestDate = struct

  module Float = struct

    type t = float
    let add = ( +. )
    let sub = ( -. )
    let mult = ( *. )
    let div = ( /. )

    let floor = Pervasives.floor

    let of_int = float_of_int

    let to_int = int_of_float
    let to_float x = x

  end

  module FDate = Date.Make(Float)

  let epsilon = 1.0e-5

  let (=.) a b = (abs_float (a-.b)) < epsilon

  let test_get_julian_day ctx = begin

    let result = FDate.get_julian_day 2016 01 01
    and expected = 42370. in

    let _msg ~expected ~result =
        Printf.sprintf "Expected %f but got %f"
        expected
        result in

    assert_equal
      ~cmp:(=.)
      ~msg:(_msg ~expected ~result)
      expected
      result
  end

  let test_from_julian_day ctx = begin

    let _msg (y1, m1, d1) (y2, m2, d2) =
      Printf.sprintf "Expected %d-%d-%d but got %d-%d-%d"
        y1 m1 d1
        y2 m2 d2
    in

    let result = FDate.date_from_julian_day 734.
    and expected = (1902, 01, 03) in

    assert_equal
      ~msg:(_msg expected result)
      expected
      result;

  end

  let test_parse_time ctx = begin
    let result = FDate.from_string "1902-01-03T12:34:56"

    and expected = 3966431. /. 5400.
        (* =2415753.52425925925925925925 *)

    and _msg ~expected ~result =
        Printf.sprintf "Expected %f but got %f"
        expected
        result in

    assert_equal
      ~cmp:(=.)
      ~msg:(_msg ~expected ~result)
      expected
      result
  end

  let test_time_from_julian_day ctx = begin

    let _msg (h1, m1, s1) (h2, m2, s2) =
      Printf.sprintf "Expected %d:%d:%f but got %d:%d:%f"
        h1 m1 s1
        h2 m2 s2
    in

    let result = FDate.time_from_julian_day @@ FDate.from_string "1902-01-03T12:34:56"
    and expected = (12, 34, 56.) in

    assert_equal
      ~msg:(_msg expected result)
      ~cmp:(fun (a, b, c) (a', b', c') -> a = a' && b = b' && c =. c')
      expected
      result
  end

  let test_time_add_hour ctx = begin


    let (result:string) = FDate.from_string "1902-01-03T12:34:56"
              |> fun x -> (x +. (1. /. 2.))
              |> FDate.to_string in

    let expected = "1902-01-04T00:34:56" in

    let msg = Printf.sprintf "Expected %s but got %s" expected result in

    assert_equal
      ~msg
      expected
      result
  end

  let test_time_add_hour2 ctx = begin


    let (result:string) = FDate.from_string "1902-01-03T12:34:56"
              |> fun x -> (x +. (3. /. 4.))
              |> FDate.to_string in

    let expected = "1902-01-04T00:34:56" in

    let msg = Printf.sprintf "Expected %s but got %s" expected result in

    assert_equal
      ~msg
      expected
      result
  end

end

let tests = "tools_test">::: [

  TestString.tests;

  "test_get_julian_day"       >:: TestDate.test_get_julian_day;
  "test_from_julian_day"      >:: TestDate.test_from_julian_day;
  "test_parse_time"           >:: TestDate.test_parse_time;
  "test_time_from_julian_day" >:: TestDate.test_time_from_julian_day;
  "test_time_add_hour"        >:: TestDate.test_time_add_hour;

 ]
