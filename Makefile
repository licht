PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
OCAMLBUILD ?= ocamlbuild
PACKAGES=dynlink,curses,camlzip,xmlm,text,str,menhirLib,zarith,base,string_dict
PATHS=src,src/odf,src/tree,src/expressions
TARGET=licht
OCB=ocamlbuild -pkgs $(PACKAGES) $(STUB_OPTIONS) $(MENHIR) -Is $(PATHS) 

MENHIR=-use-menhir

LIB  = licht
LIB_STUB = $(LIB)_stub

STUB_OPTIONS=-lflags -ccopt,-L.,-cclib,-l$(LIB_STUB)
BYTE_STUB_OPTIONS=$(STUB_OPTIONS)#,-dllib,-l$(LIB_STUB)

.PHONY: stub clean

all: $(TARGET)

stub:
	$(MAKE) -C stub LIB=$(LIB)

deps:
	opam install ocamlbuild curses camlzip xmlm ounit text menhir zarith string_dict

byte: stub
	$(OCB) main.byte

$(TARGET): stub
	$(OCB) -tags optimize\(3\) main.native
	cp -L main.native $(TARGET)

doc:
	$(OCB) licht.docdir/index.html

install: $(TARGET)
	mkdir -p $(BINDIR)
	install $(TARGET) $(BINDIR)	

uninstall:
	rm -rf $(BINDIR)/$(TARGET)

%.cmxs: stub
	$(OCB) -use-ocamlfind -tags optimize\(3\) -Is $(PATHS) $@

test: test.byte
	./test.byte

test.byte: stub
	$(OCB) -pkg oUnit -cflag -g -lflag -g -Is tests,tests/odf,tests/tree $@

relink: stub
	rm -f _build/*.native
	rm -f _build/*.byte
	make

clean:
	rm -f licht
	rm -f *.native
	rm -f *.byte
	$(MAKE) -C stub LIB=$(LIB) clean


	$(OCB) -clean

