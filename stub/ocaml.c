/*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ocaml.h"

value
get_opt(value opt, int index) {
	if (!opt || opt == Val_none)
		return 0;
	else
		return Field(opt, index);
}

char*
string_opt(const value opt) {
	value content = get_opt(opt, 0);
	if (!content)
		return NULL;
	else
		return String_val(content);
}

value
Val_some(value v ) {
    CAMLparam1( v );
    CAMLlocal1( some );
    some = caml_alloc(1, 0);
    Store_field( some, 0, v );

    CAMLreturn( some );
}

value
Val_1field(value v) {
    CAMLparam1( v );
    CAMLlocal1( field );
    field = caml_alloc(1, 0);
    Store_field(field, 0, v);

    CAMLreturn( field );
}
