/*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ncurses.h>

#include "ocaml.h"

CAMLprim value
c_set_mouse_event(value events)
{
    CAMLparam1(events);
    CAMLlocal1(event);

    mmask_t mask = 0;

    while (events != Val_emptylist)
    {
        event = Field(events, 0);

        switch (Int_val(event)) {
            case 0:  mask |= BUTTON1_PRESSED; break;
            case 1:  mask |= BUTTON1_RELEASED; break;
            case 2:  mask |= BUTTON1_CLICKED; break;
            case 3:  mask |= BUTTON1_DOUBLE_CLICKED; break;
            case 4:  mask |= BUTTON1_TRIPLE_CLICKED; break;
            case 5:  mask |= BUTTON2_PRESSED; break;
            case 6:  mask |= BUTTON2_RELEASED; break;
            case 7:  mask |= BUTTON2_CLICKED; break;
            case 8:  mask |= BUTTON2_DOUBLE_CLICKED; break;
            case 9:  mask |= BUTTON2_TRIPLE_CLICKED; break;
            case 10: mask |= BUTTON3_PRESSED; break;
            case 11: mask |= BUTTON3_RELEASED; break;
            case 12: mask |= BUTTON3_CLICKED; break;
            case 13: mask |= BUTTON3_DOUBLE_CLICKED; break;
            case 14: mask |= BUTTON3_TRIPLE_CLICKED; break;
            case 15: mask |= BUTTON4_PRESSED; break;
            case 16: mask |= BUTTON4_RELEASED; break;
            case 17: mask |= BUTTON4_CLICKED; break;
            case 18: mask |= BUTTON4_DOUBLE_CLICKED; break;
            case 29: mask |= BUTTON4_TRIPLE_CLICKED; break;
            case 20: mask |= BUTTON_SHIFT; break;
            case 21: mask |= BUTTON_CTRL; break;
            case 22: mask |= BUTTON_ALT; break;
            case 23: mask |= ALL_MOUSE_EVENTS; break;
            case 24: mask |= REPORT_MOUSE_POSITION; break;
        }
        events = Field(events, 1);
    }
    mousemask(mask, NULL);


    CAMLreturn(Val_unit);
}

CAMLprim value
c_get_mouse_event(value unit)
{
    MEVENT event;
    CAMLparam1(unit);
    CAMLlocal2(result, coord);

    result = caml_alloc(3, 0);
    coord = caml_alloc(3, 0);

    if (getmouse(&event) == OK)
    {
        Store_field(coord, 0, Val_int(event.x));
        Store_field(coord, 1, Val_int(event.y));
        Store_field(coord, 2, Val_int(event.z));

        Store_field(result, 0, Val_int(event.id));
        Store_field(result, 1, event.bstate);
        Store_field(result, 2, coord);

        CAMLreturn(Val_some(result));
    } else {
        CAMLreturn(Val_none);
    }
}


CAMLprim value
c_is_event_of_type(value mask, value type)
{
    CAMLparam2(mask, type);
    switch (Int_val(type)) {
        case 0:  CAMLreturn(Val_bool(mask & BUTTON1_PRESSED)); break;
        case 1:  CAMLreturn(Val_bool(mask & BUTTON1_RELEASED)); break;
        case 2:  CAMLreturn(Val_bool(mask & BUTTON1_CLICKED)); break;
        case 3:  CAMLreturn(Val_bool(mask & BUTTON1_DOUBLE_CLICKED)); break;
        case 4:  CAMLreturn(Val_bool(mask & BUTTON1_TRIPLE_CLICKED)); break;
        case 5:  CAMLreturn(Val_bool(mask & BUTTON2_PRESSED)); break;
        case 6:  CAMLreturn(Val_bool(mask & BUTTON2_RELEASED)); break;
        case 7:  CAMLreturn(Val_bool(mask & BUTTON2_CLICKED)); break;
        case 8:  CAMLreturn(Val_bool(mask & BUTTON2_DOUBLE_CLICKED)); break;
        case 9:  CAMLreturn(Val_bool(mask & BUTTON2_TRIPLE_CLICKED)); break;
        case 10: CAMLreturn(Val_bool(mask & BUTTON3_PRESSED)); break;
        case 11: CAMLreturn(Val_bool(mask & BUTTON3_RELEASED)); break;
        case 12: CAMLreturn(Val_bool(mask & BUTTON3_CLICKED)); break;
        case 13: CAMLreturn(Val_bool(mask & BUTTON3_DOUBLE_CLICKED)); break;
        case 14: CAMLreturn(Val_bool(mask & BUTTON3_TRIPLE_CLICKED)); break;
        case 15: CAMLreturn(Val_bool(mask & BUTTON4_PRESSED)); break;
        case 16: CAMLreturn(Val_bool(mask & BUTTON4_RELEASED)); break;
        case 17: CAMLreturn(Val_bool(mask & BUTTON4_CLICKED)); break;
        case 18: CAMLreturn(Val_bool(mask & BUTTON4_DOUBLE_CLICKED)); break;
        case 29: CAMLreturn(Val_bool(mask & BUTTON4_TRIPLE_CLICKED)); break;
        case 20: CAMLreturn(Val_bool(mask & BUTTON_SHIFT)); break;
        case 21: CAMLreturn(Val_bool(mask & BUTTON_CTRL)); break;
        case 22: CAMLreturn(Val_bool(mask & BUTTON_ALT)); break;
        case 23: CAMLreturn(Val_bool(mask & ALL_MOUSE_EVENTS)); break;
        case 24: CAMLreturn(Val_bool(mask & REPORT_MOUSE_POSITION)); break;
    }
    CAMLreturn(Val_false);
}
