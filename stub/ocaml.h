/*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OC__STUB_OCAML_H

#include <caml/custom.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/mlvalues.h>

#define Val_none Val_int(0)

value get_opt(value opt, int index);

/**
 * Convert a « string option » to char* or Null
 */
char* string_opt(const value opt);

/**
 * Store the given value as an option.
 */
value Val_some(value v);

/**
 * Create a field of one element containing the value v.
 */
value Val_1field(value v);

#endif
