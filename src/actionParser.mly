%{
  open Actions

%}

%token ESC
%token EOF
%token LEFT RIGHT UP DOWN
%token NPAGE PPAGE HOME END
%token DELETE
%token SEARCH
%token E U V Y P
%token EQUAL
%token I
%token <int*int>BUTTON1_CLICKED
%token <int*int>BUTTON1_RELEASED
%token COMMAND
%token RESIZE

%start <Actions.actions> normal
%%


normal:
  | ESC    { Escape }
  | LEFT   { Move (Left 1) }
  | RIGHT  { Move (Right 1) }
  | UP     { Move (Up 1) }
  | DOWN   { Move (Down 1) }
  | DELETE { Delete }
  | E      { Edit }
  | U      { Undo }
  | V      { Visual }
  | Y      { Yank }
  | P      { Paste }
  | SEARCH { Search }
  | EQUAL  { InsertFormula }
  | NPAGE  { Move (Down 10)}
  | PPAGE  { Move (Up 10)}
  | HOME   { Move (Left 10)}
  | END    { Move (Right 10)}
  | BUTTON1_CLICKED { Button1_clicked $1}
  | BUTTON1_RELEASED{ Button1_released $1}
  | COMMAND { Command }
  | RESIZE  { Resize }
