(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

include Text

let empty = ""

let decode x = Text.decode x

let encode x =
  try Some (Text.encode x)
  with Text.Invalid (_, _) -> None

let raw_encode x = Text.encode x

let from_utf8string x = x

let to_utf8string x = x

let trim x = Text.strip x

let split str ~sep =
  match Text.split ~max:1 ~sep str with
  | [] -> ""
  | hd::tl -> hd

let replace text patt repl = Text.replace text ~patt ~repl

module Buffer = struct

    include Buffer

    type buffer = t

    let add_char b c = Uchar.of_char c
    |> Uchar.to_int
    |> Text.char
    |> Buffer.add_string b
end

module Printf = struct

    include Printf

end

module Format = struct

  include Format

  let bprintf buffer fformat = begin
    let to_b = formatter_of_buffer buffer in
    let x = fprintf to_b fformat in
    x
  end

end
