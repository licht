(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

(** This module represent a sheet *)

type cell = int * int

type t

(** Crate an empty sheet. *)
val create: Functions.C.t -> t

(** Add or update the sheet.
    The expression is added at given position.
    @return A set containing all updated cells. *)
val add: history:bool -> Expression.t -> cell -> t -> Cell.Set.t

(** Undo the last action and return true if something has been undone *)
val undo: t -> bool

(** Delete the content of selected cells.
    @return The sheet and the number of cells deleted
*)
val delete: Selection.t -> t -> int

(** Copy the selected cells 
    @return The sheet and the number of cells deleted
*)
val yank: Selection.t -> t -> int

(** Paste all the selection at the given position. *)
val paste: cell -> t -> int

(** Fold over all the defined values *)
val fold: ('a -> cell -> (Expression.t * ScTypes.Result.t) -> 'a) -> 'a -> t -> 'a

(** Get the content from a cell.
    Also return all the other cell pointing to it.
*)
val get_cell: cell -> t -> Expression.t *  ScTypes.Result.t option * Cell.Set.t
