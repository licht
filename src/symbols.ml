(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

let u = UTF8.from_utf8string

let eq = u"="
let neq = u"<>"
let lt = u"<"
let le = u"<="
let gt = u">"
let ge = u">="

let add = u"+"
let mul = u"*"
let pow = u"^"
let div = u"/"
let sub = u"-"
