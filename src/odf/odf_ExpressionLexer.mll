(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

{
  open Odf_ExpressionParser
  open Lexing

  exception SyntaxError of string
}

let digit = ['0'-'9']
let real = digit+ | digit* '.' digit+ | digit+ '.' digit*


let newline = "\r\n" | '\n' | '\r'
let space = ['\t' ' '] | newline

let letters = ['A'-'Z' 'a'-'z']

(* Function identifier.
   Valid identifiers are : 
     ORG.OPENOFFICE.DAYSINMONTH
   it cannot end with a digit.
 *)
let identifier = letters (letters | digit | ['-' '_' '.'])* letters+

let cell = letters+ digit+

rule read = parse
  | space+   { read lexbuf }

  | digit+ as _1 { NUM _1}
  | real as _1  { REAL (Tools.String.filter_float _1)}
  | '$'         { DOLLAR }

  | '='         { EQ }
  | "<>"        { NEQ }
  | '<'         { LT }
  | "<="        { LE }
  | '>'         { GT }
  | ">="        { GE }
  | '*'         { TIMES }
  | '+'         { PLUS }
  | '-'         { MINUS }
  | '/'         { DIVIDE }
  | '"'         { read_string (Buffer.create 16) lexbuf }
  | ';'         { SEMICOLON }
  | ':'         { COLON }
  | '['         { L_SQ_BRACKET }
  | ']'         { R_SQ_BRACKET }
  | '('         { LPAREN }
  | ')'         { RPAREN }
  | '^'         { POW }
  | '.'         { DOT }

  | letters+ as _1   { LETTERS _1}
  | identifier as _1 { IDENT _1}

  | '\000'      { EOF }
  | eof         { EOF }

and read_string buf = parse
  | '"'       { STR (Buffer.contents buf) }
  | '\\' '/'  { Buffer.add_char buf '/';    read_string buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\';   read_string buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b';   read_string buf lexbuf }
  | '\\' 'f'  { Buffer.add_char buf '\012'; read_string buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n';   read_string buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r';   read_string buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t';   read_string buf lexbuf }
  | '\\' '"'  { Buffer.add_char buf '"';    read_string buf lexbuf }
  | [^ '"' '\\' '\000']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      read_string buf lexbuf
    }
  | '\000'  { STR (Buffer.contents buf) }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { STR ( Buffer.contents buf) }

and quoteless_string buf = parse
  | '\\' '/'  { Buffer.add_char buf '/';    quoteless_string buf lexbuf }
  | '\\' '\\' { Buffer.add_char buf '\\';   quoteless_string buf lexbuf }
  | '\\' 'b'  { Buffer.add_char buf '\b';   quoteless_string buf lexbuf }
  | '\\' 'f'  { Buffer.add_char buf '\012'; quoteless_string buf lexbuf }
  | '\\' 'n'  { Buffer.add_char buf '\n';   quoteless_string buf lexbuf }
  | '\\' 'r'  { Buffer.add_char buf '\r';   quoteless_string buf lexbuf }
  | '\\' 't'  { Buffer.add_char buf '\t';   quoteless_string buf lexbuf }
  | '\\' '"'  { Buffer.add_char buf '"';    quoteless_string buf lexbuf }
  | [^ '\\' '\000']+
    { Buffer.add_string buf (Lexing.lexeme lexbuf);
      quoteless_string buf lexbuf
    }
  | '\000'  { STR (Buffer.contents buf) }
  | _ { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof { STR (Buffer.contents buf) }

