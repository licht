(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

let ooo =         "http://openoffice.org/2004/office"
let ooow =        "http://openoffice.org/2004/writer"
let oooc =        "http://openoffice.org/2004/calc"
let rpt =         "http://openoffice.org/2005/report"
let tableooo =    "http://openoffice.org/2009/table"
let drawooo =     "http://openoffice.org/2010/draw"

let dc =          "http://purl.org/dc/elements/1.1/"

let xhtml =       "http://www.w3.org/1999/xhtml"
let grddl =       "http://www.w3.org/2003/g/data-view#"
let css3t =       "http://www.w3.org/TR/css3-text/"
let xlink =       "http://www.w3.org/1999/xlink"
let dom =         "http://www.w3.org/2001/xml-events"
let xforms =      "http://www.w3.org/2002/xforms"
let xsd =         "http://www.w3.org/2001/XMLSchema"
let xsi =         "http://www.w3.org/2001/XMLSchema-instance"
let math =        "http://www.w3.org/1998/Math/MathML"

let office =      "urn:oasis:names:tc:opendocument:xmlns:office:1.0"
let style =       "urn:oasis:names:tc:opendocument:xmlns:style:1.0"
let text =        "urn:oasis:names:tc:opendocument:xmlns:text:1.0"
let table =       "urn:oasis:names:tc:opendocument:xmlns:table:1.0"
let draw =        "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
let fo =          "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
let meta =        "urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
let number =      "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
let presentation= "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
let svg =         "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
let chart =       "urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
let dr3d =        "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
let form =        "urn:oasis:names:tc:opendocument:xmlns:form:1.0"
let script =      "urn:oasis:names:tc:opendocument:xmlns:script:1.0"
let oof =         "urn:oasis:names:tc:opendocument:xmlns:of:1.2"

let calcext =     "urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0"
let loext =       "urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0"

let field =       "urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0"
let formx =       "urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0"

let document_content_node = (office, "document-content")
let body_node             = (office, "body")
let spreadsheet_node      = (office, "spreadsheet")
let table_node            = (table, "table")
let table_row_node        = (table, "table-row")
  let table_row_repeat_attr = (table, "number-rows-repeated")

let table_cell_node       = (table, "table-cell")
  let number_columns_repeat_attr = (table, "number-columns-repeated")
  let cvalue_type_attr    = (calcext, "value-type")
  let ovalue_type_attr    = (office, "value-type")
  let value_attr          = (office, "value")
  let formula_attr        = (table, "formula")
  let date_value_attr     = (office, "date-value")
  let number_columns_spanned_attr = (table, "number-columns-spanned")

let text_node             = (text, "p")

let name_spaces () = [
  (Xmlm.ns_xmlns, "office"),    office;
  (Xmlm.ns_xmlns, "style"),     style;
  (Xmlm.ns_xmlns, "text"),      text;
  (Xmlm.ns_xmlns, "table"),     table;
  (Xmlm.ns_xmlns, "draw"),      draw;
  (Xmlm.ns_xmlns, "fo"),        fo;
  (Xmlm.ns_xmlns, "xlink"),     xlink;
  (Xmlm.ns_xmlns, "dc"),        dc;
  (Xmlm.ns_xmlns, "meta"),      meta;
  (Xmlm.ns_xmlns, "number"),    number;
  (Xmlm.ns_xmlns, "presentation"),presentation;
  (Xmlm.ns_xmlns, "svg"),       svg;
  (Xmlm.ns_xmlns, "chart"),     chart;
  (Xmlm.ns_xmlns, "dr3d"),      dr3d;
  (Xmlm.ns_xmlns, "math"),      math;
  (Xmlm.ns_xmlns, "form"),      form;
  (Xmlm.ns_xmlns, "script"),    script;
  (Xmlm.ns_xmlns, "ooo"),       ooo;
  (Xmlm.ns_xmlns, "ooow"),      ooow;
  (Xmlm.ns_xmlns, "oooc"),      oooc;
  (Xmlm.ns_xmlns, "dom"),       dom;
  (Xmlm.ns_xmlns, "xforms"),    xforms;
  (Xmlm.ns_xmlns, "xsd"),       xsd;
  (Xmlm.ns_xmlns, "xsi"),       xsi;
  (Xmlm.ns_xmlns, "rpt"),       rpt;
  (Xmlm.ns_xmlns, "of"),        oof;
  (Xmlm.ns_xmlns, "xhtml"),     xhtml;
  (Xmlm.ns_xmlns, "grddl"),     grddl;
  (Xmlm.ns_xmlns, "tableooo"),  tableooo;
  (Xmlm.ns_xmlns, "drawooo"),   drawooo;
  (Xmlm.ns_xmlns, "calcext"),   calcext;
  (Xmlm.ns_xmlns, "loext"),     loext;
  (Xmlm.ns_xmlns, "field"),     field;
  (Xmlm.ns_xmlns, "formx"),     formx;
  (Xmlm.ns_xmlns, "css3t"),     css3t;
]
