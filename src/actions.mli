type direction =
| Up of int
| Down of int
| Left of int
| Right of int
| Absolute of int * int

type modes =
| Normal
| Select
| Edit
| Command

type actions =
| Move of direction
| Escape
| Delete
| Yank
| Paste
| Search
| Undo
| Edit
| InsertFormula
| Visual
| Button1_clicked of (int * int)
| Button1_released of (int * int)
| Command
| Resize
