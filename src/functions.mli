(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)


(** Function signature *)

type 'a typ

val t_unit:       unit typ
val t_bool:       DataType.Bool.t typ
val t_int:        DataType.Num.t  typ
val t_string:     UTF8.t typ
val t_list: 'a typ -> 'a list typ

val typ_of_format: 'a ScTypes.DataFormat.t -> 'a typ

val repr: Format.formatter -> 'a typ -> unit

module C : Catalog.CATALOG 
  with type 'a argument = 'a typ
  and  type 'a returnType = 'a ScTypes.ReturnType.t

(** Load all the built_in functions *)
val built_in: C.catalog_builder -> C.catalog_builder
