(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

module Shift_Ref = struct

  type 'a t = (int * int) -> ScTypes.Refs.t
  type 'a obs = (int * int) -> ScTypes.Refs.t

  let _shift (vector_x, vector_y) ((x, y), (fixed_x, fixed_y)) =
    let x' = if fixed_x then x else x + vector_x
    and y' = if fixed_y then y else y + vector_y in
      (x', y'), (fixed_x, fixed_y)

  let cell t shift =
    ScTypes.Refs.cell (_shift shift t)


  let range c1 c2 shift =
    ScTypes.Refs.range (_shift shift c1) (_shift shift c2)

  let observe elem shift = elem shift

end

module Shift_Expr = struct

  type t = (int * int) -> ScTypes.Expr.t
  type obs =  (int * int) -> ScTypes.Expr.t

  module R = Shift_Ref
  module T = Id_type

  let observe f x = f x

  let value t vector = ScTypes.Expr.value (T.observe t)

  let ref r vector = ScTypes.Expr.ref (R.observe r vector)

  let call0 ident vector = ScTypes.Expr.call0 ident

  let call1 ident p1 vector = ScTypes.Expr.call1 ident (observe p1 vector)

  let call2 ident p1 p2 vector = ScTypes.Expr.call2  ident (observe p1 vector) (observe p2 vector)

  let call3 ident p1 p2 p3 vector = ScTypes.Expr.call3 ident (observe p1 vector) (observe p2 vector) (observe p3 vector)

  let expression e vector = ScTypes.Expr.expression (observe e vector)

  let callN ident (params: t list) vector = ScTypes.Expr.callN ident (List.map (fun x -> observe x vector) params)

end

include Shift_Expr
