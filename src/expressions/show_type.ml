(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

type 'a t = UTF8.Buffer.buffer -> unit
type 'a obs = UTF8.Buffer.buffer -> unit

let str s buffer =
  UTF8.Buffer.add_string buffer s

let num n buffer =
  if DataType.Num.is_integer n then
    DataType.Num.to_int n
      |> string_of_int
      |> UTF8.from_utf8string
      |> UTF8.Buffer.add_string buffer
  else
    let f = DataType.Num.to_float n
    and to_b = UTF8.Format.formatter_of_buffer buffer in
    ignore @@ UTF8.Format.fprintf to_b "%.2f" f;
    Format.pp_print_flush to_b ()

let date n buffer =
  let y, m, d = DataType.Date.date_from_julian_day n in
  UTF8.Printf.bprintf buffer "%d/%d/%d" y m d

let bool b buffer =
  UTF8.Printf.bprintf buffer "%B" b

let observe elem buffer = elem buffer
