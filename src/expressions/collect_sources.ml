(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

module T = struct

  type 'a t = unit

  type 'a obs = ('a -> 'a)

  let str s = ()

  let num n = ()

  let date d = ()

  let bool b = ()

  let observe : unit -> 'a -> 'a = fun () x -> x

end

module R = struct

  type 'a obs = Cell.Set.t -> Cell.Set.t

  type 'a t = 'a obs

  let cell : Cell.t -> Cell.Set.t -> Cell.Set.t =
    fun c set -> Cell.Set.add (Cell.to_pair c) set

  let range c1 c2 set = begin

    let x1, y1 = Cell.to_pair c1
    and x2, y2 = Cell.to_pair c2 in

    let f_x x acc = begin
      let f_y y acc = begin
        Cell.Set.add (x, y) acc
      end in
      Tools.fold_for f_y y1 y2 acc
    end in
    Tools.fold_for f_x x1 x2 set
  end

  let observe elem set = elem set


end

type obs =  Cell.Set.t -> Cell.Set.t

type t = obs

let observe (f:obs) (value :Cell.Set.t) = f value

let value v acc = T.observe v acc

let ref r acc = R.observe r acc

let call0 ident acc = acc

let call1 ident p1 acc = observe p1 acc

let call2 ident p1 p2 acc = observe p2 (observe p1 acc)

let call3 ident p1 p2 p3 acc = observe p3 (observe p2 (observe p1 acc))

let callN ident params acc = List.fold_left (fun acc p -> observe p acc) acc params

let expression e acc = e acc

