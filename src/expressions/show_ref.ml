type 'a t = UTF8.Buffer.buffer -> unit

type 'a obs = UTF8.Buffer.buffer -> unit

let cell t buffer =
  UTF8.Buffer.add_string buffer @@ Cell.to_string t

let range c1 c2 buffer =
  Tools.Tuple2.printb ~first:"" ~last:"" ~sep:":" Cell.to_buffer Cell.to_buffer buffer (c1, c2)

let observe elem buffer = elem buffer
