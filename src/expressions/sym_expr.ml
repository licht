(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

module type SYM_EXPR = sig

  module T:Sym_type.SYM_TYPE

  module R:Sym_ref.SYM_REF

  type t

  type obs

  val value : 'a T.t -> t

  val ref : 'a R.t -> t

  val call0 : UTF8.t -> t

  val call1 : UTF8.t -> t -> t

  val call2 : UTF8.t -> t -> t -> t

  val call3 : UTF8.t -> t -> t -> t -> t

  val callN: UTF8.t -> t list -> t

  val expression : t -> t

  val observe : t -> obs

end
