module T:Sym_type.SYM_TYPE

module R:Sym_ref.SYM_REF

type t

type dic = (Functions.C.t * (int * int -> ScTypes.Result.t option))

type obs = dic -> ScTypes.Result.t

val value : 'a T.t -> t

val ref : 'a R.t -> t

val call0 : UTF8.t -> t

val call1 : UTF8.t -> t -> t

val call2 : UTF8.t -> t -> t -> t

val call3 : UTF8.t -> t -> t -> t -> t

val callN: UTF8.t -> t list -> t

val expression : t -> t

val observe : t -> obs
