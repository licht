(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

let u = UTF8.from_utf8string

module Show_Expr
  (R:Sym_ref.SYM_REF with type 'a obs = (UTF8.Buffer.buffer -> unit))
  (T:Sym_type.SYM_TYPE with type 'a obs = (UTF8.Buffer.buffer -> unit)) = struct

  module T = T
  module R = R

  type t = UTF8.Buffer.buffer -> unit
  type obs = UTF8.Buffer.buffer -> unit

  let observe buffer value = buffer value

  let value v buffer = T.observe v buffer

  let ref r buffer = R.observe r buffer

  let call0 ident buffer =
    let utf8ident = UTF8.to_utf8string ident in
    UTF8.Printf.bprintf buffer "%s()" utf8ident

  let call1 ident p1 buffer =
    let utf8ident = UTF8.to_utf8string ident in
    UTF8.Printf.bprintf buffer "%s(%a)"
      utf8ident
      (fun x b -> observe b x) p1

  let call2 ident p1 p2 buffer =
    let utf8ident = UTF8.to_utf8string ident in
    begin match utf8ident with
    | "+" | "*" | "-" | "/" | "^" | "="
    | "<>" | "<=" | ">=" | "<" | ">" ->
        UTF8.Printf.bprintf buffer "%a%s%a"
        (fun x b -> observe b x) p1
          utf8ident
        (fun x b -> observe b x) p2
    | _ ->
      UTF8.Printf.bprintf buffer "%s(%a;%a)"
        utf8ident
        (fun x b -> observe b x) p1
        (fun x b -> observe b x) p2
    end

  let call3 ident p1 p2 p3 buffer =
    let utf8ident = UTF8.to_utf8string ident in
    UTF8.Printf.bprintf buffer "%s(%a;%a;%a)"
      utf8ident
      (fun x b -> observe b x) p1
      (fun x b -> observe b x) p2
      (fun x b -> observe b x) p3

  let callN ident (params: t list) buffer =
    UTF8.Buffer.add_string buffer ident;
    Tools.List.printb ~sep:(u";") (fun buffer value -> value buffer) buffer params

  let expression e buffer =
    UTF8.Printf.bprintf buffer "(%a)"
    (fun x b -> b x) e

end
