type 'a t = 'a ScTypes.Type.t

type 'a obs = 'a ScTypes.Type.t

let str s = ScTypes.Type.string s

let num n = ScTypes.Type.number n

let date d = ScTypes.Type.date d

let bool b = ScTypes.Type.boolean b

let observe x = x


