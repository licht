(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

type t =
  | Basic: 'a ScTypes.Type.t -> t      (** A direct type *)
  | Formula: formula -> t             (** A formula *)
  | Undefined: t                      (** The content is not defined *)

and formula =
  | Expression of ScTypes.Expr.t  (** A valid Expr.t *)
  | Error of int * UTF8.t             (** When the expression cannot be parsed *)


(** Load an expression *)
val load: UTF8.t -> t

val load_expr: t -> t

val is_defined: t -> bool

(** Evaluate the expression *)
val eval: t -> Functions.C.t -> ((int * int) -> ScTypes.Result.t option) -> ScTypes.Result.t

(** Collect all the cell referenced in the expression *)
val collect_sources: t -> Cell.Set.t

(** Represent an expression *)
val show: t -> UTF8.t

val shift: (int * int) -> t -> t

val (=): t -> t -> bool
