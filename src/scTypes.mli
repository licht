(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

(** All the types used in the spreadsheet. *)

(** This module describe the most basic type use in the spreadsheet : every content is,

- either a ['a t]
- either an expression which result to one ['a t]

*)
module Type : sig

  type 'a t

  (** Create a new number *)
  val number: DataType.Num.t -> DataType.Num.t t

  (** Create a new string *)
  val string: DataType.String.t -> DataType.String.t t

  (** Create a new boolean *)
  val boolean: DataType.Bool.t -> DataType.Bool.t t

  (** Create a new date *)
  val date: DataType.Num.t -> DataType.Num.t t

  val (=) : 'a t -> 'b t -> bool

  (** Evaluate a type and get the result *)
  module Eval(T:Sym_type.SYM_TYPE): sig

    val eval: 'a t -> 'a T.obs

  end

end

(** A reference to another cell.

The reference can be a single cell (ie : A$1) or a range.

*)
module Refs : sig

  type t 

  val cell : Cell.t -> t

  val range : Cell.t -> Cell.t -> t

  (** Evaluate a reference and get the result *)
  module Eval(R:Sym_ref.SYM_REF): sig

    val eval: t -> 'a R.obs

  end

end

module Expr : sig

  type ident = UTF8.t

  (** This is the cell content *)
  type t

  (** Declare a direct value *)
  val value: 'a Type.t -> t

  (** Declare a reference to another cell *)
  val ref: Refs.t -> t

  (** Declare a call to a 0 arg function *)
  val call0: ident -> t

  (** Declare a call to a 1 arg function *)
  val call1 : ident -> t -> t

  (** Declare a call to a 2 arg function *)
  val call2 : ident -> t -> t -> t

  (** Declare a call to a 3 arg function *)
  val call3 : ident -> t -> t -> t -> t

  (** Declare a call to a function *)
  val callN : ident -> t list -> t

  (** An expression *)
  val expression : t -> t

  module Eval(E:Sym_expr.SYM_EXPR): sig

    val eval: t -> E.obs

  end

end

module DataFormat : sig

  type 'a t =
    | Date: DataType.Num.t t      (* A date in julian day *)
    | Number: DataType.Num.t t    (* Number *)
    | String: DataType.String.t t (* String *)
    | Bool: DataType.Bool.t t     (* Boolean *)
  
  type formats = F : 'a t -> formats [@@unboxed]

  val default_value_for: 'a t -> 'a

  type ('a, 'b) equality = Eq : ('a, 'a) equality

  val compare_format: 'a t -> 'b t -> ('a, 'b) equality

  val priority: 'a t -> int

end

module ReturnType : sig

  (** Private type for an internal representation of return format *)
  type 'a t
  
  (** Numeric (any format) *)
  val f_num: DataType.Num.t t
  
  (** Date *)
  val f_date: DataType.Num.t t
  
  (** Number *)
  val f_number: DataType.Num.t t
  
  (** Boolean result *)
  val f_bool: DataType.Bool.t t
  
  (** String *)
  val f_string: DataType.String.t t

  val guess_format_result: 'a t -> (unit -> DataFormat.formats list) -> 'a DataFormat.t

end

module Result : sig

  (** Result from a computation *)
  type t =
    | Ok : 'a Type.t -> t
    | Error : exn -> t

  val (=) : t -> t -> bool

  val show: t -> UTF8.t

end

exception Error

