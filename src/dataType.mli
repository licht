(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

module type COMPARABLE = sig
  type t
  val eq: t -> t -> bool
  val neq: t -> t -> bool
  val lt: t -> t -> bool
  val le: t -> t -> bool
  val gt: t -> t -> bool
  val ge: t -> t -> bool
end

module Num: sig

  type t

  val one: t
  val zero: t

  val of_int: int -> t
  val to_int: t -> int

  val to_float: t -> float
  val of_float: float -> t

  val neg: t -> t

  val eq: t -> t -> bool
  val neq: t -> t -> bool
  val lt: t -> t -> bool
  val le: t -> t -> bool
  val gt: t -> t -> bool
  val ge: t -> t -> bool

  val add: t -> t -> t
  val sub: t -> t -> t
  val mult: t -> t -> t
  val div: t -> t -> t
  val pow: t -> t -> t

  val rnd: unit -> t

  val max: t -> t -> t
  val min: t -> t -> t

  val abs: t -> t

  val round: t -> t
  val floor: t -> t
  val round_down: t -> t

  val gcd: t -> t -> t
  val lcm: t -> t -> t

  val is_integer: t -> bool

end

module Bool: sig
  type t = bool

  val true_ : bool
  val false_: bool 

  val eq: t -> t -> bool
  val neq: t -> t -> bool
  val lt: t -> t -> bool
  val le: t -> t -> bool
  val gt: t -> t -> bool
  val ge: t -> t -> bool

  val not: t -> t
  val and_: t -> t -> t
  val or_: t -> t -> t
end

module String: sig
  type t = UTF8.t
  val eq: t -> t -> bool
  val neq: t -> t -> bool
  val lt: t -> t -> bool
  val le: t -> t -> bool
  val gt: t -> t -> bool
  val ge: t -> t -> bool
end

module Date: sig

  (** Create a date from a year month day *)
  val get_julian_day : int -> int -> int -> Num.t
  
  (** Return the year, month and day from a date *)
  val date_from_julian_day : Num.t -> int * int * int
  
  val time_from_julian_day : Num.t -> int * int * Num.t

  val from_string: string -> Num.t
  
  (** Print out the date *)
  val to_string: Num.t -> string
end
