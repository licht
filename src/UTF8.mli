(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

(** UTF8 is used internally for all strings

  The module is intentionaly opaque.

 *)

(** An UTF8 encoded string *)
type t

val empty: t

(** Decode a string in the current locale *)
val decode: string -> t

(** Use this to with a known UTF8 encoded string *)
val from_utf8string: string -> t

(** Encode to the string to the user locale *)
val encode: t -> string option

(** Encode the string.
    This function may raise Text.Invalid if the string cannot be encoded in current locale
*)
val raw_encode: t -> string

val to_utf8string: t -> string

val trim: t -> t

val length: t -> int

val get: t -> int -> t

val rev_explode : t -> t list

val split: t -> sep:t -> t

val fold : (t -> 'a -> 'a) -> t -> 'a -> 'a

val implode : t list -> t

val rev_implode : t list -> t

val compare: t -> t -> int

val replace: t -> t -> t -> t

val upper: t -> t

val lower: t -> t

val code: t -> int

val char: int -> t

val repeat: int -> t -> t

val get: t -> int -> t

val lchop: t -> t

val rchop: t -> t

val sub: t -> int -> int -> t

module Buffer : sig

  type buffer

  val create : int -> buffer

  val contents : buffer -> t

  val add_string : buffer -> t -> unit

  val add_char: buffer -> char -> unit

end

module Printf : sig

  val bprintf : Buffer.buffer -> ('a, Buffer.buffer, unit) format -> 'a

end

module Format: sig

  val formatter_of_buffer : Buffer.buffer -> Format.formatter

  val fprintf : Format.formatter -> ('a, Format.formatter, unit) format -> 'a

end
