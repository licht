(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

type t = (int * int) * (bool * bool)

let u = UTF8.from_utf8string

let from_string (fixed_x, x_name) (fixed_y, y) =
  let x = ref 0 in
  String.iter
    (function
      | 'a' .. 'z' as c -> x := (!x * 26) + (int_of_char c - 96)
      | 'A' .. 'Z' as c -> x := (!x * 26) + (int_of_char c - 64)
      | _ -> ())
    x_name;
  ((!x, y), (fixed_x, fixed_y))

let to_hname x =
  let rec extract acc value =
    if value > 0 then
      let value' = value - 1 in
      let rem = value' mod 26 in
      let quot = (value' - rem) / 26 in
      (extract [@tailcall]) (char_of_int (65 + rem) :: acc) quot
    else acc
  in
  let res = extract [] x and buff = UTF8.Buffer.create 4 in
  List.iter (fun c -> UTF8.Buffer.add_char buff c) res;
  UTF8.Buffer.contents buff

let to_string ((x, y), (fixed_x, fixed_y)) =
  let buff = UTF8.Buffer.create 2 in

  if fixed_x then UTF8.Buffer.add_char buff '$';
  UTF8.Buffer.add_string buff (to_hname x);
  if fixed_y then UTF8.Buffer.add_char buff '$';
  UTF8.Buffer.add_string buff @@ u @@ string_of_int y;
  UTF8.Buffer.contents buff

let to_buffer buff ((x, y), (fixed_x, fixed_y)) =
  if fixed_x then UTF8.Buffer.add_char buff '$';
  UTF8.Buffer.add_string buff (to_hname x);
  if fixed_y then UTF8.Buffer.add_char buff '$';
  UTF8.Buffer.add_string buff @@ u @@ string_of_int y

let to_string t =
  let buff = UTF8.Buffer.create 2 in
  to_buffer buff t;
  UTF8.Buffer.contents buff

let to_pair = Stdlib.fst

module Set = struct
  include Set.Make (struct
    type t = int * int

    let compare = Stdlib.compare
  end)

  let show_int_tuple b t =
    Tools.Tuple2.printb
      (fun b x -> UTF8.Buffer.add_string b @@ u (string_of_int x))
      (fun b x -> UTF8.Buffer.add_string b @@ u (string_of_int x))
      b t

  let printb buff =
    iter (fun x ->
        to_buffer buff (x, (false, false));
        UTF8.Buffer.add_char buff ' ')
end
