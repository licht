(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

(** Represent the {!module:Sheet} *)

type t

(** Run the screen *)
val run: (t -> 'a) -> 'a

(** {2 Screen updates} *)

val draw: Sheet.t -> Selection.t -> t -> unit

val draw_input: Sheet.t -> Selection.t -> t -> unit

val resize: Sheet.t -> Selection.t -> t -> t 

(** Display a message in the status bar. *)
val status: t -> UTF8.t -> unit

(** {2 User inputs} *)

(** Wait for a keycode *)
val read_key : t -> string
(** The keycode is always NULL terminated *)

(** Get the cell matching the terminal coordinates *)
val get_cell: t -> int * int -> Sheet.cell option

val editor: ?position: int * int -> ?prefix:UTF8.t -> ?init:UTF8.t -> t -> UTF8.t option
