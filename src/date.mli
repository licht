(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

module type CALCULABLE = sig

  type t

  val add: t -> t -> t

  val sub: t -> t -> t

  val mult: t -> t -> t

  val div: t -> t -> t

  val floor: t -> t

  val of_int: int -> t

  val to_int: t -> int

  val to_float: t -> float

end

module Make(C:CALCULABLE): sig

  (** Create a date from a year month day *)
  val get_julian_day : int -> int -> int -> C.t
  
  (** Return the year, month and day from a date *)
  val date_from_julian_day : C.t -> int * int * int
  
  val time_from_julian_day : C.t -> int * int * C.t

  val from_string: string -> C.t
  
  (** Print out the date *)
  val to_string: C.t -> string

end
