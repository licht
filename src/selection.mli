(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)

type t

type axe =
  | Horizontal of int
  | Vertical of int
  | Cell of (int * int)

(** Create a new selection from a cell *)
val create: (int * int) -> t

val is_selected: axe -> t -> bool

(** Get the selection origin *)
val extract: t -> (int * int)

val shift: t -> (int * int) -> (int * int)

val fold: ('a -> int * int -> 'a) -> 'a -> t -> 'a

val extends:  Actions.direction -> t -> t

val move: Actions.direction -> t -> t option
