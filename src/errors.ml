(*
This file is part of licht.

licht is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

licht is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with licht.  If not, see <http://www.gnu.org/licenses/>.
*)


(** The function is undefined *)
exception Undefined of UTF8.t * string list

exception TypeError
exception Cycle

let printf formatter = function
  | Undefined (name, args) ->
    let pp_sep f () = Format.pp_print_string f ", " in
    Format.fprintf formatter
      "There is no function '%s' with signature (%a)"
      (UTF8.to_utf8string name)
      (Format.pp_print_list ~pp_sep Format.pp_print_text) args
  | Cycle -> Format.fprintf formatter "Cycle"
  | TypeError -> Format.fprintf formatter "TypeError"
  | Not_found -> Format.fprintf formatter "Not_found"
  | _ -> Format.fprintf formatter "?"
