.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

.. default-role:: code

.. contents::
  :depth: 3

Licht is a console spreadsheet. This application allow you to view, edit, and
save spreadsheet in Open document format.

=======
Licence
=======

Licht is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

licht is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
licht. If not, see <http://www.gnu.org/licenses/>.

===========
Compilation
===========

licht requires ocaml 4.06, ncurses and gmp. The easest way to compile it is to
use opam :

.. code-block:: console

  # sudo aptitude install opam libncures-dev libiconv-dev libgmp-dev
  $ opam pin add licht http://git.chimrod.com/cgit.cgi/licht.git

=====
Usage
=====

Run licht with `licht` or `licht file.ods` in order to load a file.

Modes
=====

Licht is a modal application. When you start the spreadsheet, you are in
`Normal mode`_, which allow you to navigate in the sheet, and switch to others
modes.

Normal mode
-----------

Allow edition and sheet modification

======= ===========================
Key     Action
======= ===========================
**v**   switch to `selection mode`_
**:**   Insert commands_
**e**   edit a cell content (`edition mode`_)
**=**   insert a formula (`edition mode`_)
**y**   yank a cell
**p**   paste a cell
**DEL** delete a cell
**u**   undo the last action
======= ===========================

Commands
--------

You can insert commands with the key `:` in `Normal mode`_

=================== ======================================
Command             Action
=================== ======================================
**:enew**           Start a new spreadsheet
**:w** *filename*   Save the file with the given name
**:q**              Quit the spreadsheet. (No confirmation will be asked if the
                    spreadsheet has been modified)
=================== ======================================

Selection mode
--------------

In this mode, you can select and apply action to many cells. You can use arrow
keys to extend the selection in any direction.

========= =========================
Key       Action
========= =========================
**ESC**   go back to `normal mode`_
**y**     yank the selection
**DEL**   delete the selection
========= =========================

Edition mode
------------

Use the arrows to insert a reference to an other cell.

Data types
==========

String
------

Any value wich does not match the following types is considered as a string.

You can enter a string in formula by enclosing it with a `"` (`="this a \"quote"`).

Numeric
-------

Any numeric value can be written directly : `123`, `-.43`.

Date
----

Date are represented with this format `YYYY/MM/DD`. Any operation that can
apply to Numeric can also by applied to Date.

Boolean
-------

This type cannot be created directly. You can instead use the function `true()`
and `false()` or comparaison operator.

References
----------

You can reference cells in the formulas

Reference to a single cell
~~~~~~~~~~~~~~~~~~~~~~~~~~

You can reference a cell by naming it (`A1`, `B22`).

The value for a reference to a single cell is the value of the pointed cell.
(In case of cycle between differents cell the result is unspecified but it is
garanted to not loop.)

Range
~~~~~

Yan can reference a range by naming the two bounds (`C6:A1`).

Undefined
~~~~~~~~~

If a reference point to an an empty cell, the content will be interpreted as
Undefined. Any formula impliyng `Undefined` will return Error

Functions
=========

You can enter a formula by starting the value with `=`. The evaluation always
expands the reference to other cells.

Licht is provided with built-in functions :

Generic comparaison
-------------------

Thoses function can be applied to any value, and return a Boolean.

=============== ===============================
Function        Value
=============== ===============================
*x* `=` *y*     True if *x* equals *y*
*x* `>` *y*     True if *x* > *y*
*x* `>=` *y*    True if *x* >= *y*
*x* `<` *y*     True if *x* < *y*
*x* `<=` *y*    True if *x* <= *y*
*x* `<>` *y*    True if *x* does not equals *y*
=============== ===============================

Boolean
-------

================= ===============================
Function          Value
================= ===============================
`true()`          True
`false()`         False
`not(Bool)`       True  if the parameter is False,
                  False if the parameter is True
`and(Bool; Bool)` Logical and
`or(Bool; Bool)`  Logical or
`xor(Bool; Bool)` Logical exclusive or
================= ===============================

Condition
~~~~~~~~~

========================= ===========================================
Function                  Value
========================= ===========================================
`if(Bool`; *x* ; *y* `)`  Return *x* if `Bool` is True, otherwise *y*
========================= ===========================================

Numeric
-------

=================== =====================================
Function            Value
=================== =====================================
*x* `+` *y*         Addition
*x* `-` *y*         Difference
*x* `*` *y*         Multiplication
*x* `/` *y*         Division
*x* `^` *y*         Compute *x* ^ *y*
`gcd(` *x*; *y* `)` Greatest common divisor
`lcm(` *x*; *y* `)` Lowest common multiple
`rand()`            A random number between 0 and 1
`sqrt(Numeric)`     Square root
`exp(Numeric)`      Exponential
`ln(Numeric)`       Natural logarithm
`abs(Numeric)`      Absolute value
=================== =====================================

Rounding
~~~~~~~~

The table show the differents results for the rounding functions

===================== === === ==== ====
Function              1.2 1.8 -1.2 -1.8
===================== === === ==== ====
`int(Numeric)`        1   1   -2   -2
`rounddown(Numeric)`  1   1   -1   -1
`round(Numeric)`      1   2   -1   -2
===================== === === ==== ====

Operations on lists
~~~~~~~~~~~~~~~~~~~

=================== =====================================
Function            Value
=================== =====================================
`sum(Numeric List)` Compute the sum of the list.
`min(Numeric List)` Get the minimum value
`max(Numeric List)` Get the maximum value
=================== =====================================

Trigonometry
~~~~~~~~~~~~

All the trigonometry functions works in radian.

===================== =====================================
Function              Value
===================== =====================================
`pi()`                Get the value for :math:`\pi`
`cos(Numeric)`        Get the cosine for the value
`sin(Numeric)`        Get the sine for the value
`tan(Numeric)`        Get the tangent for the value
`acos(Numeric)`       Arc cosine
`asin(Numeric)`       Arc sine
`atan(Numeric)`       Arc tangent
`cosh(Numeric)`       Hyperbolic cosine
`sinh(Numeric)`       Hyperbolic sine
`tanh(Numeric)`       Hyperbolic tangent
`atan2(` *x*; *y* `)` Arc tangent of *x* / *y*
===================== =====================================

Text
----

================================= ======================================================
Function                          Value
================================= ======================================================
`trim(Text)`                      Remove all space on left and right
`upper(Text)`                     Convert the string to uppercase
`lower(Text)`                     Convert the string to lowercase
`unicode(Text)`                   Returns the numeric code for the first Unicode character
`unichar(Numeric)`                Converts a code number into a Unicode character or letter
`substitute(Text, Text, Text)`    Substitutes new text for old text in a string
`len(Text)`                       Return the length of a string
`left(Text, Numeric)`             Return the first n characters
`right(Text, Numeric)`            Return the last n characters
================================= ======================================================
